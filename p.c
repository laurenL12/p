#include <stdio.h>

#define PROGRAM_NAME  "p"

void usage(FILE *f);

int main(int argc, char **argv)
{
    if (argc < 2) {
        usage(stderr);
        return 1;
    }

    char *filename = argv[1];
    FILE *f = fopen(filename, "r");
    if (f == NULL) {
        printf("%s: %s: No such file or directory\n", PROGRAM_NAME, filename);
        return 1;
    }
    char c = 0;

    while ((c = fgetc(f)) != EOF) {
        printf("%c", c);
    }

    fclose(f);
    return 0;
}

void usage(FILE *f)
{
    fprintf(f, "usage: %s <file>\n", PROGRAM_NAME);
}


Print file contents

Based on UNIX's `p` command.

## Building
```console
$ cc p.c -o p
```
